# Interview Task

- [1. Objectives](#1-objectives)
- [2. Prerequisites](#2-prerequisites)
- [3. Deployment](#3-deployment)
  - [3.1. Summary](#31-summary)
  - [3.2. Walkthrough](#32-walkthrough)
- [4. Development](#4-development)
- [5. Testing](#5-testing)
- [6. Contributing](#6-contributing)
- [7. License](#7-license)

## 1. Objectives

Thank you for taking the time to do our technical test. We need to deploy a new .NET Core Web API application using a docker container.

Write code to do the following:

1. Run the automated tests
2. Package the application as a docker image
3. Deploy and run the image locally or in a public cloud

Improvements can also be made. For example:

- Make any changes to the application you think are useful for a deploy process
- Host the application in a secure fashion

Your solution should be triggered by a powershell script called `Deploy.ps1`.

## 2. Prerequisites

- [.NET Core SDK 3.1 (LTS)](https://dotnet.microsoft.com/download)
- [Docker Desktop](https://www.docker.com/products/docker-desktop)

## 3. Deployment

Clone this repository and execute [deploy.ps1](deploy.ps1) to build and deploy a containerized ASP .NET Core application:

```powershell
pwsh -ExecutionPolicy ByPass deploy.ps1
```

### 3.1. Summary

1. Validate unit tests.
1. Build image from Dockerfile.
1. Deploy container locally (or, optionally, to remote).

### 3.2. Walkthrough

[![Interview task video walkthrough.](video_thumbnail.png)](https://www.youtube.com/watch?v=2cKB_xEXJTk)

## 4. Development

```bash
# Install dependencies
dotnet restore ./src

# Run build
dotnet build -p ./src

# Access https://localhost:5001/time
```

## 5. Testing

```bash
# Install dependencies
dotnet restore ./test

# Run tests
dotnet test ./test
```

## 6. Contributing

Pull requests are more than welcome. For major changes, please open an issue first for discussion.

## 7. License

[The Unlicense](LICENSE)
