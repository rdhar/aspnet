'STARTED Deployment Script' && `
  '01 VALIDATE Tests' && `
  dotnet test ./test && `
  '02 BUILD Image' && `
  docker build -f ./src/Dockerfile --target production -t rdhar/aspnet:latest . && `
  '03 DEPLOY Container' && `
  docker run --rm -itp 8080:80 rdhar/apsnet:latest && `
  # docker push rdhar/aspnet:latest && `
  'COMPLETED Deployment Script'
